# Usage

```
docker run -d --name sandbox sneak/sandbox ; 
docker exec -ti sandbox script -q -c "TERM=xterm-color byobu" /dev/null
```

(The 'script' bit works around a bug where `docker exec -t` doesn't
give you a usable tty.)

Or, if you're clever and run an Ubuntu mirror (or caching reverse proxy) on
your docker host:

```
docker run -d --add-host archive.ubuntu.com:172.17.0.1 --name sandbox sneak/sandbox ; 
docker exec -ti sandbox script -q -c "TERM=xterm-color byobu" /dev/null
```

# Includes Things Like

* vim
* neovim
* rbenv
* default-jre
* docker (from docker)
* docker-compose
* docker-machine
* bash completion
* python (from ubuntu)
    * setuptools
    * virtualenv
* node4 (from ubuntu)
    * coffeescript
    * coffeelint
* go1.6 (from ubuntu)
* byobu and screen
* awscli (from pypi)
* irssi
* runit / daemontools
    * envdir
* build-essential
    * make
* latex and fonts (from ubuntu)
* Other useful misc tools
    * pv
    * pbzip2
    * nmap
    * ppss
    * pwgen
    * netcat and telnet
    * and much more...

# TODO

* ctags / enhanced vim
* sweet screenrc or byobu config
* customize PS1
* more shells
* more editors (emacs)
* rbenv build versions
* more fonts
* pathogen
* overcommit
* mercurial

# See Also

* https://github.com/marvinpinto/kitchensink
* https://github.com/shykes/devbox

# Feedback

Feedback is appreciated, PRs are encouraged.

Drop me an email or tweet [@sneakdotberlin](https://twitter.com/sneakdotberlin).

# Author

Jeffrey Paul &lt;sneak@sneak.berlin&gt;

https://sneak.berlin

[@sneakdotberlin](https://twitter.com/sneakdotberlin)

`5539 AD00 DE4C 42F3 AFE1  1575 0524 43F4 DF2A 55C2`

# License

This code is released into the public domain.
