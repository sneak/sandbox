#!/bin/bash
export DEBIAN_FRONTEND=noninteractive

USERNAME="sneak"

set -x
set -e
exec 1> >(tee -a /var/log/sandboxbuild.log) 2>&1

PKGS="
    apt-transport-https
    apt-utils
    aspell
    aspell-en
    automake
    bash-completion
    bc
    bind9-host
    bonnie++
    build-essential
    byobu
    cmake
    command-not-found
    curl
    daemontools
    debmirror
    default-jre
    diffstat
    dnsutils
    docker.io
    dos2unix
    editorconfig
    ffmpeg
    fonts-indic
    fonts-ipafont-gothic
    fonts-ipafont-mincho
    fortune
    git
    gnupg-agent
    gnupg2
    golang-go
    host
    imagemagick
    iputils-ping
    irssi
    jq
    lcov
    ldap-auth-client
    ldap-utils
    libasound2
    libatk1.0-0
    libboost-all-dev
    libdb++-dev
    libgconf-2-4
    libgtk-3-0
    libjpeg-dev
    libnss-mdns
    libnss3
    libpng-dev
    libssl-dev
    libtool
    libxcursor1
    libxml2
    libxml2-dev
    libxslt1-dev
    locales
    lsof
    mailutils
    make
    man
    man-db
    mercurial
    mosh
    mutt
    netcat-openbsd
    nmap
    nscd
    openssh-server
    pass
    pbzip2
    pinentry-curses
    pkg-config
    pkg-config
    psmisc
    pv
    pwgen
    python
    python3
    python-dev
    python3-dev
    python-pip
    python3-pip
    rbenv
    rsync
    rsyslog
    rsyslog-gnutls
    rsyslog-relp
    runit
    screen
    snmp
    snmpd
    software-properties-common
    strace
    sudo
    tcpdump
    telnet
    texlive-latex-base
    tmux
    tree
    ttf-wqy-microhei
    ttf-wqy-zenhei
    vagrant
    vim
    vim-gtk
    wamerican-insane
    wget
    xterm
    zip
"

apt update
apt -y upgrade
apt install -y $PKGS

# install neovim
add-apt-repository ppa:neovim-ppa/unstable
apt update
apt install -y neovim

mkdir -p /etc/bashrc.d
mkdir -p /etc/profile.d

cat >> /etc/bash.bashrc <<'EOF'
if ! shopt -oq posix; then
    if [ -f /usr/share/bash-completion/bash_completion ]; then
        . /usr/share/bash-completion/bash_completion
    elif [ -f /etc/bash_completion ]; then
        . /etc/bash_completion
    fi
fi

for FN in /etc/bashrc.d/*.sh; do
    source "$FN"
done

if ! shopt -oq posix; then
    for FN in /etc/profile.d/*.sh; do
        source "$FN"
    done
fi
EOF

################################################################################
## install nvm + node
################################################################################

export NVM_DIR='/usr/local/nvm'
mkdir -p "$NVM_DIR"
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
cat > /etc/bashrc.d/100.nvm.sh <<'EOF'
source /usr/local/nvm/nvm.sh
EOF
source /etc/bashrc.d/100.nvm.sh

nvm install node
nvm use node

################################################################################
## install yarn
################################################################################
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg |
    apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" |
    tee /etc/apt/sources.list.d/yarn.list
apt update && apt install -y yarn

################################################################################
## install rbenv + ruby
################################################################################
git clone https://github.com/rbenv/rbenv.git /usr/local/rbenv
cat > /etc/bashrc.d/100.rbenv.sh <<'EOF'
export PATH="/usr/local/rbenv/bin:$PATH"
eval "$(rbenv init -)"
EOF
source /etc/bashrc.d/100.rbenv.sh

mkdir -p "$(rbenv root)"/plugins
git clone https://github.com/rbenv/ruby-build.git "$(rbenv root)"/plugins/ruby-build

rbenv install $(rbenv install -l | grep -v - | tail -1)
#rbenv install 2.6.3
#rbenv install jruby-9.0.5.0

# install some gems
GEMS="
    bundler
"

for GEM in $GEMS ; do
    gem install -V $GEM
done

################################################################################
## install ppss, paralell processing shell script
################################################################################
cd /usr/local/bin
wget https://raw.githubusercontent.com/sneak/ppss/master/ppss
chmod +x ./ppss

################################################################################
## upgrade pip and install bare essentials
## update other python packages as possible
################################################################################
#pip3 install --upgrade pip
pip3 install setuptools
#pip3 install pip-review
#pip-review --verbose --auto

# install other python packages
PIP_PKGS="
    awscli
    pipenv
    pylint
    virtualenv
"

for PKG in $PIP_PKGS; do
    pip3 install $PKG
done

################################################################################
## add working user and add to sudo nopassword
################################################################################
groupadd -g 1000 $USERNAME
useradd -u 1000 -g 1000 -s /bin/bash $USERNAME
usermod -p '*' $USERNAME
echo "$USERNAME ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
