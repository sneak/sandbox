YYYYMMDD := $(shell date +%Y%m%d)
BRANCH := $(shell git branch | sed -n -e 's/^\* \(.*\)/\1/p')

default: build

build:
	script -q ./build.log docker build --no-cache -t sneak/sandbox:$(YYYYMMDD) .
